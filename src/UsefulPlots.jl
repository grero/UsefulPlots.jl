module UsefulPlots
using LinearAlgebra
using Colors
using SomeCoolColourMaps
using Makie
using Makie: Point2f

include("plotutils.jl")

@recipe(ConfidenceEllipse, μ, Σ) do scene
    Theme(color = "red",
          label = ""
         ) 
end

"""
Plot the mean and the confidence ellipse of the multivate gaussian distribution parametrised by `μ` and `Σ`
"""
function Makie.plot!(plot::ConfidenceEllipse)
    μ = plot[1][]
    Σ = plot[2][]
    u,s,v = svd(Σ)
    Q = u*Diagonal(sqrt.(s))*v'
    npoints = 100 
    x = fill(0.0, npoints)
    y = fill(0.0, npoints)
    Δϕ = 2π/npoints
    for i in 1:npoints-1
        ϕ = (i-1)*Δϕ
        _x = cos(ϕ)
        _y = sin(ϕ)
        #apply rotation and scaling
        x[i] = _x*Q[1,1] - _y*Q[1,2] + μ[1]
        y[i] = _x*Q[2,1] + _y*Q[2,2] + μ[2]
    end
    x[end] = x[1]
    y[end] = y[1]
    lines!(plot, x,y, color=plot[:color])
    scatter!(plot, [μ[1]], [μ[2]], color=plot[:color])
end

end # module
