@recipe(Wedges, θ, Δθ, origin, r) do scene
    Theme(colormap = cmap("C2"))
end

function Makie.plot!(p::Wedges)
    origin = p[3][]
    θ = p[1][]
    Δθ = p[2][]
    r = p[4][]
    polys = Vector{Point2f}[]
    _cm = p[:colormap][]
    colors = _cm[1:div(length(_cm),length(θ)):length(_cm)]
    for i in 1:length(θ)
        pp = [Point2f(origin[1] + r[1]*cos(ϕ), origin[2] + r[2]*sin(ϕ)) for ϕ in range(θ[i]- Δθ/2, stop=θ[i] + Δθ/2, length=20)]
        pp = [[origin];pp;[origin]]
        push!(polys, pp)
        poly!(p, pp, color=colors[i])
    end
end

@recipe(Plane, points) do scene
    Theme()
end

function Makie.plot!(plot::Plane)
    points = plot[1]
    #define triangles
    tridx = [1,2,3,1,4,3]
    mesh!(plot,points, tridx)
end

@recipe(StackedBuckets, level, height) do scene
    Theme(colormap = cmap("C2"),
          vertical = true,
          xoffset = 0.0,
          yoffset = 0.0)
end

function Makie.plot!(plot::StackedBuckets)
    bheight = plot[1][]
    blevel = plot[2][]
    vertical = plot[:vertical][]
    xoffset = plot[:xoffset][]
    yoffset = plot[:yoffset][]
    _cm = plot[:colormap][]
    colors = _cm[1:div(length(_cm),length(bheight)):length(_cm)]
    for (h,l,color) in zip(bheight, blevel,colors)
        if vertical
            Δh = Point2f(1.0, h)
            Δl = Point2f(1.0, l)
        else
            Δh = Point2f(h, 1.0)
            Δl = Point2f(l, 1.0)
        end
        rrh = Rect(xoffset, yoffset,Δh...)
        rrl = Rect(xoffset, yoffset, Δl...)
        poly!(plot, rrh, color=RGBA(0.2, 0.2, 0.2,0.0) + 0.8*color)
        poly!(plot, rrl, color=color)
        if vertical
            yoffset += h
        else
            xoffset += h
        end
    end
end
