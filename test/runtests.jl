using UsefulPlots
using Colors
using LinearAlgebra
using Makie
using Makie: Point2f, Point3f
using Test

@testset "Gaussians" begin
    μ = [0.0, 0.0]
    Σ = [1.0 0.3;0.3 0.8]

    fig, ax, plotobject = confidenceellipse(μ, Σ, color="blue")
    @test plotobject.plots[1][1][][10] ≈ Float32[0.74724966, 0.6071399]
    @test plotobject.plots[1][1][][50] ≈ Float32[-0.9951465, -0.10513139]
end

@testset "Wedges" begin
    θ = range(-pi, stop=pi, length=10)
    Δθ = step(θ)
    fig,ax,plotobject = wedges(θ, Δθ, Point2f(0.0, 0.0), Point2f(1.0, 1.0))
    @test plotobject.plots[1].color[] ≈ RGBA{Float64}(0.9531719984702666,0.3073822792529262,0.965837667236174,1.0)
    @test plotobject.plots[1][1][][1] ≈ Point2f(0.0)
    @test plotobject.plots[1][1][][end-1] ≈ Point2f(-0.939693, -0.34202) 

end

@testset "Plane" begin
    points = [Point3f(-1.0, -1.0, -1.0), Point3f(-1.0, 1.0, -1.0), Point3f(1.0, 1.0, 1.0), Point3f(1.0, -1.0, 1.0)]
    fig,ax,plotobject = plane(points)
    @test plotobject.plots[1][1][].position ≈ Point{3,Float32}[[-1.0, -1.0, -1.0], [-1.0, 1.0, -1.0], [1.0, 1.0, 1.0], [1.0, -1.0, 1.0]]
    @test plotobject.plots[1][1][].normals[2] ≈ Float32[0.707107, 0.0, -0.707107]
    @test plotobject.plots[1][1][].normals[4] ≈  Float32[-0.707107, 0.0, 0.707107]
end

@testset "StackedBuckets" begin
    h = 10*rand(4)
    l = rand(4).*h
    fig,ax,plotobject = stackedbuckets(h,l)
    @test widths(plotobject.plots[1][1][]) ≈ Point2f(1.0, h[1])
    @test widths(plotobject.plots[2][1][]) ≈ Point2f(1.0, l[1])
    @test top(plotobject.plots[3][1][]) ≈ Float32(h[1]) + Float32(h[2])
    @test top(plotobject.plots[4][1][]) ≈ Float32(h[1]) + Float32(l[2])

    fig,ax,plotobject = stackedbuckets(h,l, vertical=false)
    @test widths(plotobject.plots[1][1][]) ≈ Point2f(h[1], 1.0)
    @test widths(plotobject.plots[2][1][]) ≈ Point2f(l[1], 1.0)
    @test right(plotobject.plots[3][1][]) ≈ Float32(h[1]) + Float32(h[2])
    @test right(plotobject.plots[4][1][]) ≈ Float32(h[1]) + Float32(l[2])
    h = 10*rand(4)
    l = rand(4).*h

    fig,ax,plotobject = stackedbuckets(h,l)
    h = 10*rand(4)
    l = rand(4).*h
    plotobject = stackedbuckets!(ax, h,l, xoffset=1.5)
    @test top(plotobject.plots[1][1][]) ≈ Float32(h[1])
    @test right(plotobject.plots[1][1][]) ≈ 2.5

end
